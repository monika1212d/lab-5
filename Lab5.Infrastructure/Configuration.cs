﻿using System;
using PK.Container;


namespace Lab5.Infrastructure
{
    public class Configuration
    {
        /// <summary>
        /// Konfiguruje komponenty używane w aplikacji
        /// </summary>
        /// <returns>Kontener ze zdefiniowanymi komponentami</returns>
        public static IContainer ConfigureApp()
        {
            IContainer Kontener = new Container.Implementation.Container();
            Kontener.Register(typeof(Display.Implementation.Display));
            Kontener.Register(typeof(Main.Implementation.Program));
            return Kontener;
        }
    }
}
