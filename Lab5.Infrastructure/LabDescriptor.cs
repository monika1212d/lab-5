﻿using System;
using System.Reflection;
using Lab5.DisplayForm;
using Lab5.Infrastructure;

namespace Lab5.Infrastructure
{
    public struct LabDescriptor
    {
        #region P1

        public static Type Container = typeof(Container.Implementation.Container);

        #endregion

        #region P2

        public static Assembly MainComponentSpec = Assembly.GetAssembly(typeof(Main.Contract.IProgram));
        public static Assembly MainComponentImpl = Assembly.GetAssembly(typeof(Main.Implementation.Program));

        public static Assembly DisplayComponentSpec = Assembly.GetAssembly(typeof(Display.Contract.IDisplay));
        public static Assembly DisplayComponentImpl = Assembly.GetAssembly(typeof(Display.Implementation.Display));

        #endregion
    }
}
