﻿using System;
using System.Windows;

using PK.Container;
using Lab5.Infrastructure;

using Lab5.DisplayForm;
using Main.Contract;
using Display.Implementation;


namespace Lab5.Main
{
    public class Program
    {
        [STAThread]
        static void Main(string[] args)
        {
            /* Uruchomienie wątku dla aplikacji okienkowej */
            ApplicationStart();

            // Należy zaimplementować tę metodę
            IContainer container = Configuration.ConfigureApp();

            /*** Początek własnego kodu ***/

            //DisplayFormExample();
            IProgram program = container.Resolve(typeof(IProgram)) as IProgram;
           
            {
                program.jeden();
                System.Threading.Thread.Sleep(1500);
                program.dwa();
                System.Threading.Thread.Sleep(1500);
                program.trzy();
                System.Threading.Thread.Sleep(1500);
                program.cztery();
                System.Threading.Thread.Sleep(1500);
            }

            Console.ReadKey();

            /*** Koniec własnego kodu ***/

            /* Zatrzymanie wątku dla aplikacji okienkowej */
            ApplicationStop();
        }

        private static void DisplayFormExample()
        {
            /* Uruchomienie kodu w wątku aplikacji okienkowej */
            var model = (DisplayViewModel)Application.Current.Dispatcher.Invoke(new Func<DisplayViewModel>(() =>
                {
                    // utworzenie nowej formatki graficznej stanowiącej widok
                    var form = new Form();
                    // utworzenie modelu widoku (wzorzec MVVM)
                    var viewModel = new DisplayViewModel();
                    // przypisanie modelu do widoku
                    form.DataContext = viewModel;
                    // wyświetlenie widoku
                    form.Show();
                    // zwrócenie modelu widoku do dalszych manipulacji
                    return viewModel;
                }), null);

            /* Modyfikacja właściwości modelu - zmiana napisu na wyświetlaczu */
            model.Text = "Test";
        }

        public static void ApplicationStart()
        {
            var thread = new System.Threading.Thread(CreateApp);
            thread.SetApartmentState(System.Threading.ApartmentState.STA);
            thread.Start();
            System.Threading.Thread.Sleep(300);
        }

        private static void CreateApp()
        {
            var app = new Application();
            app.ShutdownMode = ShutdownMode.OnExplicitShutdown;
            app.Run();
        }

        public static void ApplicationStop()
        {
            Application.Current.Dispatcher.InvokeShutdown();
        }

    }
}

