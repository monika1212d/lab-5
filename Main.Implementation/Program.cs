﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Main.Contract;
using Display.Contract;
using Display.Implementation;


namespace Main.Implementation
{
    public class Program : IProgram
    {
        IDisplay display;
        public Program(IDisplay display)
        {
            this.display = display;
        }

        public void jeden()
        {
            string wynik = String.Format("Its gonna be LEGEN...");
            display.Pokaz(wynik);
        }
        public void dwa()
        {
            string wynik = String.Format("...wait for it...");
            display.Pokaz(wynik);
        }

        public void trzy()
        {
            string wynik = String.Format("...DARY!");
            display.Pokaz(wynik);
        }
        public void cztery()
        {
            string wynik = String.Format("LEGENDARY!!");
            display.Pokaz(wynik);
        }
    }
}
