﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Display.Implementation;
using Lab5.DisplayForm;
using Display.Contract;
using System.Windows;

namespace Display.Implementation
{
    public class Display : IDisplay
    {
       Lab5.DisplayForm.DisplayViewModel model;
        public void Pokaz(string text)
        {
            model.Text = text;
        }
        public Display()
        {
            model = (DisplayViewModel)Application.Current.Dispatcher.Invoke(new Func<DisplayViewModel>(() =>
            {
                var form = new Form();
                var viewModel = new DisplayViewModel();
                form.DataContext = viewModel;
                form.Show();
                return viewModel;
            }), null);
        }
    }
}
